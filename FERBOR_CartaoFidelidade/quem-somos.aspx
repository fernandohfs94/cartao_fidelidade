﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="quem-somos.aspx.cs" Inherits="FERBOR_CartaoFidelidade.quem_somos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="corpo-quemsomos">
        <article id="descricao">
            <header>
                <hgroup>
                    <h5><a href="/index.aspx">Home</a> > Quem Somos</h5>
                    <h2>A Empresa</h2>
                </hgroup>
                <p>
                        A FERBOR foi criada em 2015 por dois estudantes de Sistemas de Informação,
                        Fernando Soares e Rafael Bastazini. O cartão de fidelidade foi desenvolvido
                        para aprimorar a estratégia de Marketing de micro e pequenas empresas
                        comerciais, proporcionando uma ferramenta eficiente na fidelização de clientes.
                    </p>
                    <figure class="foto-legenda">
                        <img src="_imagens/cartao-marrom.png" alt="Modelo 1"/>
                        <figcaption>
                            Ferbor Brown - Modelo 1
                        </figcaption>
                    </figure>
                    <figure class="foto-legenda">
                        <img src="_imagens/cartao-verde.png" alt="Modelo 2"/>
                        <figcaption>
                            Ferbor Green - Modelo 2
                        </figcaption>
                    </figure>
            </header>

            <h1>Missão</h1>
                <p>
                    Aumentar a prosperidade de pequenas e médias empresas oferecendo um produto que conduza
                    à fidelização de seus clientes, aumentando a renda e a satisfação do comércio.
                </p>
            <h1>Visão</h1>
                <p>
                    Popularizar o nosso produto em Franca e região, de tal forma que a pontuação em um comércio
                    possa trazer benefício também a outros que utilizam o sistema.
                </p>
        </article>
    </section>
    <aside id="lateral">
        <h1>Valores</h1>
           <ul id="lista">
               <li>Honestidade</li>
               <li>Sinceridade</li>
               <li>Companheirismo</li>
               <li>Trabalho duro</li>
               <li>Comprometimento</li>
               <li>Dedicação</li>
               <li>Fidelidade</li>
           </ul>
        <h1>Vídeo Institucional</h1>
            <div align="center">
                <iframe width="540" height="310" src="http://www.wideo.co/embed/9259341429635354170?height=300&width=500"
                align="middle" frameborder="0"></iframe>
            </div>
    </aside>
</asp:Content>
