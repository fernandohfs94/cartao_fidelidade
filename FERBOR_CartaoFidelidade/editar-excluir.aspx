﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="editar-excluir.aspx.cs" Inherits="FERBOR_CartaoFidelidade.editar_excluir" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <header id="breadcrumb">
            <hgroup>
                <h5><a href="index.aspx">Home</a> > <a href="cadastro.aspx">Cadastro</a> > Editar/Excluir</h5>
            </hgroup>
        </header>
        <asp:GridView ID="gvwDadosCliente" runat="server" style="position: relative; top: 100px; left: 100px;" CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="id_cliente"
         AutoGenerateEditButton="true" AutoGenerateColumns="false" OnRowEditing="EditRecord" OnRowUpdating="UpdateRecord" OnRowCancelingEdit="CancelRecord"
         OnRowDeleting="DeleteRecord" PageSize="5" Font-Names="Trebuchet MS" Height="233px" Width="1200px">
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2ff1BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="id_cliente" HeaderText="ID" ReadOnly="True"  SortExpression="id_cliente" />
                <asp:TemplateField HeaderText="Nome" SortExpression="nome">
                    <ItemTemplate>
                        <%# Eval("nome") %>
                    </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNome" runat="server" Text='<%# Eval("nome") %>' 
                                Height="22px" Width="202px"></asp:TextBox>
                        </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CPF" SortExpression="cpf">
                    <ItemTemplate>
                            <%# Eval("cpf") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCpf" runat="Server" Text='<%# Eval("cpf") %>' 
                            Height="24px" Width="203px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sexo" SortExpression="sexo">
                    <ItemTemplate>
                            <%# Eval("sexo") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtSexo" runat="Server" Text='<%# Eval("sexo") %>' 
                            Height="24px" Width="203px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="E-mail" SortExpression="email">
                    <ItemTemplate>
                            <%# Eval("email") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEmail" runat="Server" Text='<%# Eval("email") %>' 
                            Height="24px" Width="203px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CEP" SortExpression="cep">
                    <ItemTemplate>
                            <%# Eval("cep") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCep" runat="Server" Text='<%# Eval("cep") %>' 
                            Height="24px" Width="150px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Logradouro" SortExpression="logradouro">
                    <ItemTemplate>
                            <%# Eval("logradouro") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtLogradouro" runat="Server" Text='<%# Eval("logradouro") %>' 
                            Height="24px" Width="203px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nº" SortExpression="numero">
                    <ItemTemplate>
                            <%# Eval("numero") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtNumero" runat="Server" Text='<%# Eval("numero") %>' 
                            Height="24px" Width="130px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Estado" SortExpression="estado">
                    <ItemTemplate>
                            <%# Eval("estado") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtEstado" runat="Server" Text='<%# Eval("estado") %>' 
                            Height="24px" Width="203px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cidade" SortExpression="cidade">
                    <ItemTemplate>
                            <%# Eval("cidade") %>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCidade" runat="Server" Text='<%# Eval("cidade") %>' 
                            Height="24px" Width="180px"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Deletar?">
                    <ItemTemplate>
                        <span onclick="return confirm('Deseja realmente Excluir este item ?')">
                            <asp:LinkButton ID="lkbDeletar" runat="server" Text="Deletar" CommandName="Delete"></asp:LinkButton>
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <asp:Label ID="lblMensagem" runat="server" Font-Bold="True" ForeColor="#CC0000" 
                style="font-family: Aharoni; position: relative; top: 103px; left: 200px;"></asp:Label>
    </form>
</asp:Content>
