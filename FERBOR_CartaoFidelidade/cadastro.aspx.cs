﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FERBOR_CartaoFidelidade
{
    public partial class cadastro : System.Web.UI.Page
    {
        crud_cadastro objCRUD = new crud_cadastro();

        #region Page_Load
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        #endregion

        #region Métodos

            #region ShowMessage

        public void ShowMessage(string mensagem)
        {
            mensagem = mensagem.Replace("'", "''").Replace("\n", "");

            System.Web.UI.
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "__Mensagem", "alert('" + mensagem + "');", true);
        }

        #endregion

            #region LimparCampos
        
        public void LimparCampos()
        {
            cNome.Text = "";
            cCPF.Text = "";
            cEmail.Text = "";
            cCEP.Text = "";
            cLogradouro.Text = "";
            cNumero.Text = "";
            cEstado.SelectedIndex = 0;
            cCidade.Text = "";
        }
        
        #endregion

        #endregion

        #region btnEnviar

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            var nome = String.Empty;
            var cpf = String.Empty;
            var sexo = String.Empty;
            var email = String.Empty;
            var cep = String.Empty;
            var logradouro = String.Empty;
            var numero = String.Empty;
            var estado = String.Empty;
            var cidade = String.Empty;

            if (cNome.Text != "")
            {
                nome = cNome.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha o nome completo.");
            }

            if (cCPF.Text != "")
            {
                cpf = cCPF.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha o CPF.");
            }

            if ((oMasc.Enabled == false) && (oFem.Enabled == false))
            {
                ShowMessage("Por favor, escolha o sexo.");
            }
            else
            {
                sexo = gSexo.SelectedItem.Text;
            }

            if (cEmail.Text != "")
            {
                email = cEmail.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha seu e-mail.");
            }

            if (cCEP.Text != "")
            {
                cep = cCEP.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha o CEP.");
            }

            if (cLogradouro.Text != "")
            {
                logradouro = cLogradouro.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha o Logradouro.");
            }

            if (cNumero.Text != "")
            {
                numero = cNumero.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha o Nº.");
            }

            if (cEstado.SelectedIndex != 0)
            {
                estado = cEstado.SelectedItem.Text;
            }
            else
            {
                ShowMessage("Por favor, selecione o Estado.");
            }

            if (cCidade.Text != "")
            {
                cidade = cCidade.Text;
            }
            else
            {
                ShowMessage("Por favor, preencha a Cidade.");
            }

            if (Convert.ToInt32(cQuantidade.Text) == 0)
            {
                ShowMessage("A quantidade deve ser maior do que 0.");
            }

            var vGravou = objCRUD.inserirDadosCliente(nome, cpf, sexo, email, cep, logradouro, Convert.ToInt32(numero), estado, cidade);

            if (vGravou)
            {
                ShowMessage("Dados inseridos com sucesso!");
                LimparCampos();
            }
            else
            {
                ShowMessage("Houve um erro ao inserir os dados.");
            }
        }
        
        #endregion
    }
}