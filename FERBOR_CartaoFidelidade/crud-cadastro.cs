﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FERBOR_CartaoFidelidade
{
    public class crud_cadastro
    {
        conexao_database objConexao = new conexao_database();

        public bool inserirDadosCliente(string nome, string cpf, string sexo, string email, string cep, string logradouro, int numero, string estado, string cidade)
        {
            return objConexao.executaInsertUpdateDelete("INSERT INTO tb_cliente(nome, cpf, sexo, email, cep, logradouro, numero, estado, cidade) VALUES('" + nome + "', '" + cpf + "', '" + sexo + "', '" + email + "', '" + cep + "', '" + logradouro + "', '" + numero + "', '" + estado + "', '" + cidade + "')");
        }

        public bool atualizarDadosCliente(int idCliente, string nome, string cpf, string sexo, string email, string cep, string logradouro, int numero, string estado, string cidade)
        {
            return objConexao.executaInsertUpdateDelete("UPDATE tb_cliente SET nome = '" + nome + "', cpf = '" + cpf + "', sexo = '" + sexo + "', email = '" + email + "', cep = '" + cep + "', logradouro = '" + logradouro + "', numero = '" + numero + "', estado = '" + estado + "', cidade = '" + cidade + "' WHERE id_cliente = " + idCliente + ";");
        }

        public bool deletaDadosCliente(int idCliente)
        {
            return objConexao.executaInsertUpdateDelete("DELETE FROM tb_cliente WHERE id_cliente = " + idCliente + ";");
        }

        public DataTable retornaDadosCliente()
        {
            return objConexao.executaSelect("SELECT * FROM tb_cliente ORDER BY id_cliente");
        }
    }
}