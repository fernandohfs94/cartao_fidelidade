﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FERBOR_CartaoFidelidade
{
    public partial class editar_excluir : System.Web.UI.Page
    {
        DataTable objTabela = null;

        #region Métodos

        /// <summary>
        /// Disparado quando o botão Cancel for clicado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            gvwDadosCliente.EditIndex = -1;
            BindGrid();
        }

        /// <summary>
        /// Disparado quando o botão Edit for clicado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditRecord(object sender, GridViewEditEventArgs e)
        {
            gvwDadosCliente.EditIndex = e.NewEditIndex;
            BindGrid();
        }

        private DataTable BindGrid()
        {
            crud_cadastro objCRUD = new crud_cadastro();

            try
            {
                objTabela = objCRUD.retornaDadosCliente();
                gvwDadosCliente.DataSource = objTabela;
                gvwDadosCliente.DataBind();
            }
            catch (Exception ee)
            {
                lblMensagem.Text = ee.Message.ToString();
            }
            finally
            {
                objTabela = null;
            }
            return objTabela;
        }

        protected void DeleteRecord(object sender, GridViewDeleteEventArgs e)
        {
            int personID = Int32.Parse(gvwDadosCliente.DataKeys[e.RowIndex].Value.ToString());

            crud_cadastro objCRUD = new crud_cadastro();

            try
            {
                bool retorno = objCRUD.deletaDadosCliente(personID);

                lblMensagem.Text = "Registro deletado com sucesso.";
            }
            catch (Exception ee)
            {
                lblMensagem.Text = ee.Message.ToString();
            }
            finally
            {
                objCRUD = null;
            }

            gvwDadosCliente.EditIndex = -1;
            // atualiza a lista
            BindGrid();
        }

        protected void UpdateRecord(object sender, GridViewUpdateEventArgs e)
        {
            int codigo = Int32.Parse(gvwDadosCliente.DataKeys[e.RowIndex].Value.ToString());
            bool resultado;
            GridViewRow row = gvwDadosCliente.Rows[e.RowIndex];

            TextBox tNome = (TextBox)row.FindControl("txtNome");
            TextBox tCpf = (TextBox)row.FindControl("txtCpf");
            TextBox tSexo = (TextBox)row.FindControl("txtSexo");
            TextBox tEmail = (TextBox)row.FindControl("txtEmail");
            TextBox tCep = (TextBox)row.FindControl("txtCep");
            TextBox tLogradouro = (TextBox)row.FindControl("txtLogradouro");
            TextBox tNro = (TextBox)row.FindControl("txtNumero");
            TextBox tEstado = (TextBox)row.FindControl("txtEstado");
            TextBox tCidade = (TextBox)row.FindControl("txtCidade");
            

            crud_cadastro objCRUD = new crud_cadastro();

            try
            {
                //resultado = pBAL.Update(codigo, tNome.Text, tEmail.Text, int.Parse(tIdade.Text));
                resultado = objCRUD.atualizarDadosCliente(codigo, tNome.Text, tCpf.Text, tSexo.Text, tEmail.Text, tCep.Text, tLogradouro.Text, Convert.ToInt32(tNro.Text), tEstado.Text, tCidade.Text);
                if (resultado == true)
                    lblMensagem.Text = "Registro atualizado com sucesso.";
                else
                    lblMensagem.Text = "Registro não pode ser atualizado.";
            }
            catch (Exception ee)
            {
                lblMensagem.Text = ee.Message.ToString();
            }
            finally
            {
                objCRUD = null;
            }

            gvwDadosCliente.EditIndex = -1;
            // atualiza a lista
            BindGrid();
        }

        /// <summary>
        /// Disparado quando um link for clicado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangePage(object sender, GridViewPageEventArgs e)
        {
            gvwDadosCliente.PageIndex = e.NewPageIndex;
            // atualiza a lista
            BindGrid();
        }

        /// <summary>
        /// Pega o GridView DataSource
        /// </summary>
        private DataTable GridDataSource()
        {
            crud_cadastro objCRUD = new crud_cadastro();
            DataTable tabela = new DataTable();
            try
            {
                tabela = objCRUD.retornaDadosCliente();
            }
            catch (Exception ee)
            {
                lblMensagem.Text = ee.Message.ToString();
            }
            finally
            {
                objCRUD = null;
            }

            return tabela;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindGrid();
        }
    }
}