﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="cadastro.aspx.cs" Inherits="FERBOR_CartaoFidelidade.cadastro" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <header id="breadcrumb">
        <hgroup>
            <h5><a href="index.aspx">Home</a> > Cadastro</h5>
        </hgroup>
    </header>
    <section id="corpo">
        <article id="form-cadastro">
            <header>
                <hgroup>
                    <h4>Preencha os dados abaixo para receber seu cartão!</h4>
                </hgroup>
            </header>
            <form id="cadastro" method="post" runat="server">
                <fieldset id="user">
                    <legend>Dados Pessoais</legend>
                    <p>
                       <label for="cNome">Nome:</label>
                       <asp:TextBox ID="cNome" runat="server" placeholder="Nome Completo"></asp:TextBox>
                    </p>
                    <p id="cpf">
                        <label for="cCPF">CPF:</label>
                        <asp:TextBox ID="cCPF" runat="server" MaxLength="11" placeholder="Somente Números"></asp:TextBox>
                    </p>
                    <fieldset id="sexo">
                        <legend>Sexo</legend>
                        <asp:RadioButtonList ID="gSexo" runat="server">
                            <asp:ListItem ID="oMasc" Value="Masculino" runat="server"/>
                            <asp:ListItem ID="oFem" Value="Feminino" runat="server" />
                        </asp:RadioButtonList>
                    </fieldset>
                    <p>
                        <label for="cEmail">E-mail:</label>
                        <asp:TextBox ID="cEmail" runat="server" placeholder="example@email.com.br"></asp:TextBox>
                    </p>
                </fieldset>
                <br />
                <fieldset id="address">
                    <legend>Endereço</legend>
                    <p>
                        <label for="cCEP">CEP:</label>
                        <asp:TextBox ID="cCEP" CssClass="campoCEP" runat="server" MaxLength="8" placeholder="Somente números"></asp:TextBox>
                    </p>
                    <p id="log">
                        <label for="cLogradouro">Endereço:</label>
                        <asp:TextBox ID="cLogradouro" CssClass="campoLog" runat="server" placeholder="Rua, Av, Rodovia, etc..."></asp:TextBox>
                    </p>
                    <p id="nro">
                        <label for="cNumero">Nº:</label>
                        <asp:TextBox ID="cNumero" runat="server" min="0" TextMode="Number"></asp:TextBox>
                    </p>
                    <p>
                        <label for="cEstado">Estado:</label>
                        <asp:DropDownList ID="cEstado" runat="server">
                            <asp:ListItem Value="select">Selecione o estado</asp:ListItem>
                            <asp:ListItem Value="SP">São Paulo</asp:ListItem>
                            <asp:ListItem Value="MG">Minas Gerais</asp:ListItem>
                            <asp:ListItem Value="RJ">Rio de Janeiro</asp:ListItem>
                            <asp:ListItem Value="DF">Distrito Federal</asp:ListItem>
                            <asp:ListItem Value="ES">Espírito Santo</asp:ListItem>
                            <asp:ListItem Value="MS">Mato Grosso do Sul</asp:ListItem>
                            <asp:ListItem Value="RS">Rio Grande do Sul</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <label for="cCidade">Cidade:</label>
                        <asp:TextBox ID="cCidade" runat="server"></asp:TextBox>
                    </p>
                </fieldset>
                <br />
                <fieldset id="pedido">
                    <legend>Quero receber um FERBOR em casa!</legend>
                    <p>
                        <label for="cCor">Cor do cartão:</label>
                        <asp:TextBox ID="cCor" runat="server" TextMode="Color"></asp:TextBox>
                    </p>
                    <p>
                        <label for="cQuantidade">Quantidade:</label>
                        <asp:TextBox ID="cQuantidade" runat="server" TextMode="Number" min="0" max="5" onChange="calc_Total()"></asp:TextBox>
                    </p>
                    <%--<p>
                        <label for="cTotal">Valor total R$:</label>
                        <asp:TextBox ID="cTotal" runat="server" CssClass="campoTotal" placeholder="Total a pagar"></asp:TextBox>
                    </p>--%>
                    <br />
                    <p>
                        <label for="cGrau">Grau de urgência:</label>
                        Mínimo
                        <asp:TextBox ID="cGrau" runat="server" TextMode="Range"></asp:TextBox>
                        Máximo
                    </p>
                </fieldset>
                <br />
                <asp:Button ID="btnEnviar" runat="server" CssClass="botao-enviar" Text="Enviar" OnClick="btnEnviar_Click" />
            </form>
            <br />
            <p class="editar-excluir"><a href="editar-excluir.aspx">Clique aqui</a> para editar ou excluir cadastros (Área Administrativa).</p>
        </article>
    </section>
</asp:Content>
