﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="especificacoes.aspx.cs" Inherits="FERBOR_CartaoFidelidade.especificacoes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="corpo-specs">
        <article id="specs">
           <header>
              <hgroup>
                 <h5> <a href="index.aspx">Home</a> > Especificações </h5>
                 <h2> Veja as especificações do cartão fidelidade FERBOR </h2>
              </hgroup>
            </header>

				<p>
					Clique em qualquer área destacada da imagem para ter mais informações sobre as especificações do cartão fidelidade FERBOR.
				</p>

			<section id="cartao">
				<img src="_imagens/cartao-specs.png" usemap="meumapa"/>
        <map name="meumapa">
          <area shape="rect" coords="87,83, 153,165" href="/cartao-specs.html#chip" target="janela"/>
          <area shape="rect" coords="332,32, 413,70" href="cartao-specs.html#loja" target="janela"/> 
          <area shape="rect" coords="401,126, 486,205" href="cartao-specs.html#seguranca" target="janela"/>
          <area shape="rect" coords="76,172, 157,206" href="cartao-specs.html#numero" target="janela"/>
          <area shape="rect" coords="236,242, 280,261" href="cartao-specs.html#validade" target="janela"/>
          <area shape="rect" coords="61,258, 257,297" href="cartao-specs.html#nome" target="janela"/>
          <area shape="rect" coords="412,282, 466,353" href="cartao-specs.html#logo" target="janela"/>
        </map>
				<iframe id="texto-spec" src="cartao-specs.html" name="janela">
				</iframe>
			<section>
		</article>
	  </section>
</asp:Content>
