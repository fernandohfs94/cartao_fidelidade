﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="cartao-fidelidade.aspx.cs" Inherits="FERBOR_CartaoFidelidade.cartao_fidelidade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   Só para quem esteve conosco de verdade!
Copyright 2015 - by Fernando Henrique and Rafael Bordon
FERBOR > O que é Cartão Fidelidade
 <section id="cartao-fidelidade">
        <article id="cartao">
           <header>
              <hgroup>
                 <h5> <a href="index.aspx">Home</a> > O que é Cartão Fidelidade </h5>
                 <h2> O que é o Cartão Fidelidade FERBOR </h2>
              </hgroup>
            </header>

          <p>
            O cartão FERBOR funciona como um cartão de crédito, utilizando até os mesmos equipamentos das outras operadoras.
              Porém, a transação não acontece em dinheiro, mas em pontos obtidos de acordo com a política de cada loja, podendo
              ser por quantidade comprada, sorteios, promoções e premiações.
          </p>
            
          <ul id="album">
            <li id="foto1"><span>Modelo 1 - Ferbor Brown</span></li>
            <li id="foto2"><span>Modelo 2 - Ferbor Green</span></li>
            <li id="foto3"><span>Funciona em qualquer maquininha</span></li>
            <li id="foto4"><span>Ofereça aos seus clientes</span></li>
            <li id="foto5"><span>Premie os clientes mais fiéis</span></li>
            <li id="foto6"><span>Veja suas vendas aumentarem</span></li>
          </ul>

            <p>
                <table id="tabela">
                  <caption>
                    Características do Cartão Fidelidade
                  </caption>
                  <tr>
                    <td class="ce">
                      Medidas:
                    </td>
                    <td class="cd">
                      7cm x 5cm
                    </td>
                  </tr>
                  <tr>
                    <td class="ce">
                      Material:
                    </td>
                    <td class="cd">
                      Plástico
                    </td>
                  </tr>
                  <tr>
                    <td class="ce">
                      Funcionamento:
                    </td>
                    <td class="cd">
                      Qualquer aparelho de cartão
                    </td>
                  </tr>
                </table>
            </p>
        </article>
	</section>
</asp:Content>
