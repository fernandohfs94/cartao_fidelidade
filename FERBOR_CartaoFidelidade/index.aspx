﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="FERBOR_CartaoFidelidade.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="index">
        <article id="home">
            <img id="banner-principal" src="_imagens/banner-principal.png" width="90%"/>
        </article>

        <ul id="menu">
            <a href="quem-somos.aspx"><li id="foto1"><span>Quem Somos</span></li></a>
            <a href="cartao-fidelidade.aspx"><li id="foto2"><span>O que é Cartão Fidelidade</span></li></a>
            <a href="cadastro.aspx"><li id="foto3"><span>Cadastro</span></li></a>
            <a href="especificacoes.aspx"><li id="foto4"><span>Especificações</span></li></a>
          </ul>

    </section>
</asp:Content>
