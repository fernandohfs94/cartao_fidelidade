﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace FERBOR_CartaoFidelidade
{
    public class conexao_database
    {
        #region Variáveis

        static string serverName = "127.0.0.1";
        static string port = "5432";
        static string userName = "postgres";
        static string password = "rafael";
        static string databaseName = "FERBOR_CARTAO_FIDELIDADE";
        NpgsqlConnection pgsqlConnection = null;
        string connString = null;
        
        #endregion

        #region Métodos

        #region Método que faz a conexão com o banco de dados
        
        public conexao_database()
        {
            connString = String.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};", serverName, port, userName, password, databaseName);
        }

        #endregion

        #region Método que abre a conexão com o BD e executa comandos insert, update e delete
        
        public bool executaInsertUpdateDelete(string comandoSql)
        {
            try
            {
                NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

                NpgsqlCommand pgsqlCommand = new NpgsqlCommand(comandoSql, pgsqlConnection);

                pgsqlCommand.Connection.Open();

                pgsqlCommand.ExecuteNonQuery();

                pgsqlCommand.Connection.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        #endregion

        #region Método que abre a conexão com o BD e executa comando select
        
        public DataTable executaSelect(String comandoSql)
        {
            DataSet objDataSet = new DataSet("objDataSet");

            NpgsqlConnection pgsqlConnection = new NpgsqlConnection(connString);

            NpgsqlCommand pgsqlCommand = new NpgsqlCommand(comandoSql, pgsqlConnection);

            NpgsqlDataAdapter pgsqlAdpt = new NpgsqlDataAdapter(pgsqlCommand);

            pgsqlAdpt.Fill(objDataSet, "tbDados");

            return objDataSet.Tables["tbDados"];
        }
        
        #endregion

        #endregion
    }
}