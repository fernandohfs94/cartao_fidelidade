﻿-- CRIAÇÃO DO BANCO DE DADOS
CREATE DATABASE "FERBOR_CARTAO_FIDELIDADE"
TEMPLATE = TEMPLATE0
ENCODING 'WIN1252'
CONNECTION LIMIT 2;

-- CRIAÇÃO DAS TABELAS
-- 'tb_cliente'
CREATE TABLE tb_cliente(
 id_cliente	SERIAL,
 nome		VARCHAR(60),
 cpf		VARCHAR(11),
 sexo		VARCHAR(30),
 email		VARCHAR(80),
 cep		VARCHAR(8),
 logradouro	VARCHAR(80),
 numero		INTEGER,
 estado		VARCHAR(30),
 cidade		VARCHAR(30),
CONSTRAINT pk_tb_cliente_id_cliente PRIMARY KEY(id_cliente)
);
-- 'tb_pedido'
CREATE TABLE tb_pedido(
 id_pedido	SERIAL,
 id_cliente	INTEGER,
 cor		VARCHAR(15),
 quantidade	INTEGER,
 valor_total	NUMERIC(4,2),
 grau		INTEGER,
CONSTRAINT pk_tb_pedido_id_pedido PRIMARY KEY(id_pedido),
CONSTRAINT fk_tb_pedido_id_cliente FOREIGN KEY(id_cliente)
	REFERENCES tb_cliente(id_cliente)
);